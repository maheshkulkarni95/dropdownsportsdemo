//
//  SectionData.swift
//  DropDownSportsDemo
//
//  Created by scmc-mac3 on 20/11/21.
//

import UIKit


struct SectionData {
    var open: Bool
    var data: [CellData]
}

struct CellData {
    var title: String
    var featureImage: UIImage
    
}
