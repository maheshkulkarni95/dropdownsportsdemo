//
//  ViewController.swift
//  DropDownSportsDemo
//
//  Created by scmc-mac3 on 13/11/21.
//

import UIKit

class TableViewController: UITableViewController {
    
    private let CELL_IDENTIFIER = "cell"
    
    fileprivate var sections: [SectionData] = [
    SectionData(
        open: false,
        data: [
            CellData(title: "Instagram", featureImage: UIImage(named: "0")!),
            CellData(title: "FaceBook", featureImage: UIImage(named: "1")!),
            CellData(title: "Whats App", featureImage: UIImage(named: "2")!),
            ]
    ),
    SectionData(
        open: false,
        data:[
            CellData(title: "Instagram", featureImage: UIImage(named: "0")!),
            CellData(title: "Whats App", featureImage: UIImage(named: "1")!),
            CellData(title: "Facebook", featureImage: UIImage(named: "2")!),
            ]
        ),
    SectionData(
        open: false,
        data:[
            CellData(title: "Instagram", featureImage: UIImage(named: "0")!),
            CellData(title: "Whats App", featureImage: UIImage(named: "1")!),
            CellData(title: "Facebook", featureImage: UIImage(named: "2")!),
            ]
        )
    ]

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        view.backgroundColor = UIColor.init(red: 228/255, green: 230/255, blue: 234/255, alpha: 1.0)
        navigationItem.title = "Sports"
        tableView.separatorStyle = .none
        registerCell()
    }
    
    func registerCell() {
        tableView.register(CardCell.self, forCellReuseIdentifier: CELL_IDENTIFIER)
    }
    
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let button = UIButton()
        button.setTitle("Open", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.tag = section
        button.addTarget(self, action: #selector(openSection), for: .touchUpInside)
        return button
    }
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if !sections[section].open {
          return 0
        }
        return sections[section].data.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER, for: indexPath) as? CardCell else {
            return UITableViewCell()
        }
        let section = sections[indexPath.section]
        let cellData = section.data[indexPath.row]
        cell.cellData = cellData
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 240
    }
    
    @objc func openSection(button: UIButton) {
        print("Button Tag: \(button.tag)")
        let section = button.tag
        
        var indexPath = [IndexPath]()
        
        for row in sections[section].data.indices {
            let indexPathToDelete = IndexPath(row: row, section: section)
            indexPath.append(indexPathToDelete)
        }
       let isOpen = sections[section].open
        sections[section].open = !isOpen
        
       // tableView.reloadData()
        button.setTitle(isOpen ? "Open" : "Close", for: .normal)
        
        if isOpen {
            tableView.deleteRows(at: indexPath, with: .fade)
        } else {
            tableView.insertRows(at: indexPath, with: .fade)
        }
    }
}


